#!/bin/sh -e
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith
# Create a test image to be used with scripts in the test dir

SCRIPT_DIR="$(dirname "$(realpath "$0")")"
TEMP_DIR="$SCRIPT_DIR/../../_temp"
WORK_DIR="$(pmbootstrap -q config work)"

if ! [ -d "$WORK_DIR" ]; then
	echo "ERROR: couldn't find pmbootstrap work dir. Make sure you"
	echo "installed pmbootstrap and ran 'pmbootstrap init'."
	exit 1
fi

ARG_BUILD_BOOT_DEPLOY=0
ARG_DEVICE="qemu-amd64"
ARG_UI="none"
ARG_VERBOSE_INITFS=0

parse_args() {
	while getopts 'bd:u:vh' OPTION; do
		case "$OPTION" in
		b)
			echo "Building boot-deploy"
			ARG_BUILD_BOOT_DEPLOY=1
			;;
		d)
			echo "Building device=$OPTARG"
			ARG_DEVICE="$OPTARG"
			;;
		u)
			echo "Building UI=$OPTARG"
			ARG_UI="$OPTARG"
			;;
		v)
			echo "Including verbose initfs hook"
			ARG_VERBOSE_INITFS=1
			;;
		h|*)
			echo "usage: $(basename $0) [-b] [-d DEVICE] [-u UI] [-v] [-h]"
			echo "arguments:"
			echo "  -b       build boot-deploy from source"
			echo "  -d       device (default: qemu-amd64)"
			echo "  -u       set user interface (default: none)"
			echo "  -v       include verbose initfs hook"
			echo "  -h       show help"
			exit 1
			;;
		esac
	done
}

parse_args "$@"

set -x
pmbootstrap -q config device "$ARG_DEVICE"
pmbootstrap -q config ui "$ARG_UI"
pmbootstrap -q config kernel virt
pmbootstrap -y -q zap -p

if [ "$ARG_BUILD_BOOT_DEPLOY" = 1 ]; then
	pmbootstrap build --src="$SCRIPT_DIR"/../../../boot-deploy boot-deploy
fi

PKGS="postmarketos-base-nofde,unl0kr"

if [ "$ARG_VERBOSE_INITFS" = 1 ]; then
	PKGS="$PKGS,postmarketos-mkinitfs-hook-verbose-initfs"
fi

pmbootstrap -q install \
	--add "$PKGS" \
	--split \
	--password=147147

mkdir -p "$TEMP_DIR"
cp "$WORK_DIR/chroot_native/home/pmos/rootfs/$ARG_DEVICE-root.img" \
	"$TEMP_DIR"/os_image.img

set +x
echo "Done!"
