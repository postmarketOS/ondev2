#!/bin/sh -e
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith

SCRIPT_DIR="$(dirname "$(realpath "$0")")"
ONDEV2="$(realpath "$SCRIPT_DIR"/..)/build/ondev2"
TEMP_DIR="$SCRIPT_DIR/../_temp"
OS_IMAGE="$TEMP_DIR/os_image.img"
STORAGE_INSTALLER="$TEMP_DIR/storage_installer.img"
STORAGE_INSTALLER_MARKER="$TEMP_DIR/.marker.storage_installer.img"
STORAGE_INSTALLER_DEV=""
STORAGE_SECOND="$TEMP_DIR/storage_second.img"
STORAGE_SECOND_DEV=""
MNT_INSTALL_BOOT="$TEMP_DIR/mnt/install_boot"
OD2_PROC_SELF_MOUNTS="$TEMP_DIR/proc-self-mounts"

ARG_ALL_CASES=0
ARG_INTERACTIVE=0
ARG_CASES=""
ARG_CASES_DEFAULT="01-sd-to-sd-simple"
ARG_GDB=""
ARG_SKIP_BUILD=0
ARG_VALGRIND=""

mkdir -p "$TEMP_DIR"

if ! [ -e "$OS_IMAGE" ]; then
	echo "ERROR: missing OS image: $OS_IMAGE"
	echo "Either put one there or create a pmOS image with pmbootstrap by"
	echo "running distro-specific/postmarketOS/create-test-image.sh"
	exit 1
fi

parse_args() {
	while getopts 'aic:ghsv' OPTION; do
		case "$OPTION" in
		a)
			echo "Running all cases"
			ARG_ALL_CASES=1
			;;
		i)
			echo "Loading one case config and running interactively"
			ARG_INTERACTIVE=1
			;;
		c)
			echo "Running case: $OPTARG"
			ARG_CASES="$ARG_CASES $OPTARG"
			;;
		g)
			# SIGILL: needed because we link against openssl
			( echo "handle SIGILL nostop"
			  echo "run" ) > "$TEMP_DIR/script.gdb"
			ARG_GDB="gdb -x $TEMP_DIR/script.gdb --args"
			;;
		s)
			ARG_SKIP_BUILD=1
			;;
		v)
			ARG_VALGRIND="valgrind"
			;;
		h|*)
			echo "usage: $(basename $0) [-a] [-i] [[-c CASE] [-c CASE2 ... ]] [-g] [-h] [-s] [-v]"
			echo "arguments:"
			echo "  -a       run all tests"
			echo "  -i       load case config only and go interactive"
			echo "  -c CASE  run this specific case (default: $ARG_CASES_DEFAULT)"
			echo "  -g       use gdb"
			echo "  -h       show help"
			echo "  -s       skip generating a new install image if possible"
			echo "  -v       use valgrind"
			exit 1
			;;
		esac
	done

	if [ "$ARG_ALL_CASES" = 1 ]; then
		if [ -n "$ARG_CASES" ]; then
			echo "ERROR: can't combine -a with -c"
			exit 1
		fi

		for i in test/cases/*.cfg; do
			ARG_CASES="$ARG_CASES $(basename "$i" | cut -d. -f 1)"
		done
	fi

	if [ -z "$ARG_CASES" ]; then
		ARG_CASES="$ARG_CASES_DEFAULT"
	fi
}

check_loop_dev() {
	if ! [ -e "$1" ]; then
		set +x
		echo "ERROR: loop device does not exist: $1"
		exit 1
	fi

	case "$1" in
		/dev/loop*)
			return
			;;
		*)
			set +x
			echo "ERROR: this does not look like a loopdev: $1"
			exit 1
	esac
}

storage_installer_prepare() {
	local initial=1

	# Reuse existing image if possible (only replaces boot partition)
	if [ "$ARG_SKIP_BUILD" = 1 ] && \
	   [ -e "$STORAGE_INSTALLER" ] && \
	   [ "$STORAGE_INSTALLER_MARKER" -nt "$OS_IMAGE" ]; then
		initial=0
	fi

	# Create a fake installer image with:
	# - p1: boot partition
	# - p2: empty space (for install to same storage use case)
	# - p3: install data
	if [ "$initial" = 1 ]; then
		truncate -s 3072M "$STORAGE_INSTALLER"
	fi

	STORAGE_INSTALLER_DEV="$(sudo losetup -f --show "$STORAGE_INSTALLER")"
	check_loop_dev "$STORAGE_INSTALLER_DEV"

	if [ "$initial" = 1 ]; then
		_parted="sudo parted -s $STORAGE_INSTALLER_DEV"
		$_parted mktable msdos
		$_parted mkpart primary ext2 2048s 200M
		$_parted mkpart primary 200M 2000M
		$_parted mkpart primary 2000M 100%
		$_parted set 1 boot on
	fi

	sudo partprobe "$STORAGE_INSTALLER_DEV"

	sudo mkfs.ext2 -F "${STORAGE_INSTALLER_DEV}p1"

	mkdir -p "$MNT_INSTALL_BOOT"
	sudo mount "${STORAGE_INSTALLER_DEV}p1" "$MNT_INSTALL_BOOT"

	if [ "$initial" = 1 ]; then
		sudo dd if="$OS_IMAGE" of="${STORAGE_INSTALLER_DEV}p3" bs=100M
		touch "$STORAGE_INSTALLER_MARKER"
	fi

	echo "${STORAGE_INSTALLER_DEV}p1 /boot ext2 ro 0 0" \
		> "$OD2_PROC_SELF_MOUNTS"
}

storage_second_prepare() {
	# Create a second storage. How it is used depends on the test case,
	# for example for SD-to-eMMC: SD is installer, eMMC is second.
	truncate -s 3072M "$STORAGE_SECOND"

	STORAGE_SECOND_DEV="$(sudo losetup -f --show "$STORAGE_SECOND")"
	check_loop_dev "$STORAGE_SECOND_DEV"
}

storage_clean_up_losetup() {
	loop_dev="$(sudo losetup -j "$1" | cut -d : -f 1)"
	if [ -n "$loop_dev" ]; then
		check_loop_dev "$loop_dev"
		sudo losetup -d "$loop_dev"
	fi
}

storage_prepare() {
	storage_installer_prepare
	storage_second_prepare
}

storage_clean_up() {
	sudo umount /tmp/ondev2/root/boot || true
	sudo umount /tmp/ondev2/root || true
	sudo cryptsetup luksClose ondev2_root || true

	storage_clean_up_losetup "$STORAGE_INSTALLER"
	storage_clean_up_losetup "$STORAGE_SECOND"
	rm -rf "$STORAGE_SECOND"

	sudo umount "$TEMP_DIR"/mnt/* || true
	rm -rf "$TEMP_DIR/mnt"

	# Clean up mountpoints created by ondev2
	sudo umount /tmp/ondev2/installdata || true
	sudo rmdir /tmp/ondev2/installdata || true
	sudo rmdir /tmp/ondev2/root || true
	sudo rmdir /tmp/ondev2 || true
}

run_test() {
	local i
	local test_exp="$SCRIPT_DIR/cases/$1.exp"
	local test_cfg="$SCRIPT_DIR/cases/$1.cfg"

	for i in $test_exp $test_cfg; do
		if ! [ -e "$i" ]; then
			set +x
			echo "ERROR: file not found: $i"
			exit 1
		fi
	done

	sed \
		-e "s#@TEST_STORAGE_INSTALLER@#$STORAGE_INSTALLER_DEV#g" \
		-e "s#@TEST_STORAGE_SECOND@#$STORAGE_SECOND_DEV#g" \
		"$test_cfg" > "$TEMP_DIR/ondev2.cfg"

	# Don't reboot the host system during this test
	echo "cmd_reboot = killall ondev2" >> "$TEMP_DIR/ondev2.cfg"
	echo "cmd_poweroff = killall ondev2" >> "$TEMP_DIR/ondev2.cfg"

	export OD2_PROC_SELF_MOUNTS
	export OD2_BOOT_MOUNTPOINT_INSTALLER="$MNT_INSTALL_BOOT"
	export PATH="$TEMP_DIR/destdir/usr/bin/:$PATH"

	preserve="OD2_PROC_SELF_MOUNTS,OD2_BOOT_MOUNTPOINT_INSTALLER,PATH"

	if [ "$ARG_INTERACTIVE" = 0 ]; then
		sudo \
			--preserve-env="$preserve" \
			$ARG_VALGRIND \
			expect -c "spawn $ARG_GDB $ONDEV2 $TEMP_DIR/ondev2.cfg" "$test_exp"
	else
		sudo \
			--preserve-env="$preserve" \
			$ARG_VALGRIND \
			$ARG_GDB \
			"$ONDEV2" "$TEMP_DIR"/ondev2.cfg

	fi

	set +x
	echo "---"
	echo "TEST SUCCESSFUL!"
	echo
	set -x
}

parse_args "$@"
set -x
sudo modprobe loop || true
meson . build --werror -Dunl0kr:werror=false
meson compile -C build
meson install --destdir "$TEMP_DIR/destdir/" -C build
for i in $ARG_CASES; do
	storage_clean_up
	storage_prepare
	run_test "$i"
done
storage_clean_up
