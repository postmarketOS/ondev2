# Contributing to ondev2

## Repo initialization

Initialize the repository with submodules:

```
$ git submodule update --init --recursive
```

## Build on the host (optional)

Build ondev2 directly on the host (mostly for ensuring it compiles, running it
directly on the host while another OS is booted is discouraged unless you know
what you are doing, as it may shutdown your PC or write to your disk and
overwrite data).

### Install dependencies

Fedora:
```
$ dnf install inih-devel libblkid-devel cryptsetup-devel libdrm-devel libudev-devel libinput-devel libxkbcommon-devel
```

### Build

```
$ abuild-meson . build -Dwerror=true -Dunl0kr:werror=false
$ meson compile -C build
```

## Build in pmbootstrap and run the testsuite

Configure qemu-amd64 and build a test OS image once (this will be installed by
the installer):

```
$ pmbootstrap config device qemu-amd64
$ distro-specific/postmarketOS/create-test-image.sh
```

Run the testsuite with the current ondev2 source (see -h for options):

```
$ test/run-test-pmbootstrap-qemu.sh
```

As of writing, you need ondev2 branches of pmbootstrap and pmaports.
