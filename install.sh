#!/bin/sh

SCRIPTDIR="$(dirname "$(readlink -f "$0")")"

install -Dm755 "$SCRIPTDIR"/src/scripts/ondev2-fstab-remove-boot-root.sh \
	"$DESTDIR"/usr/bin/ondev2-fstab-remove-boot-root
