#!/bin/sh -e
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith
# Description: run checkpatch against relevant files in the repository
# Options: native
# https://postmarketos.org/pmb-ci
# Run natively (no pmbootstrap chroot) as it only needs perl + wget and so it
# downloads checkpatch files only once.
SCRIPT_DIR="$(dirname "$(realpath "$0")")"

# Download checkpatch from linux.git
if ! [ -e "$SCRIPT_DIR/checkpatch.pl" ]; then
	cd "$SCRIPT_DIR"
	url="https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/plain/scripts/checkpatch.pl"
	echo "Download: $url"
	wget -q "$url"
	chmod +x "checkpatch.pl"
	touch spelling.txt
	cd ..
fi

# NOTE: giving all files directly as arguments to checkpatch.pl was buggy, so
# call it once per file for now. See .checkpatch.conf for additional options.

err=0
for i in src/*.c src/*.h .ci/*.sh; do
	./.ci/checkpatch.pl \
		--color=always \
		--mailback \
		--show-types \
		--showfile \
		--no-summary \
		--terse \
		"$i" || err=1
done

if [ "$err" = 1 ]; then
	echo
	echo "Please fix the errors/warnings above."
	echo
	echo "Run the script locally with:"
	echo "  .ci/lint.sh"
	echo
fi

exit $err
