#!/bin/sh -ex
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith
# Description: compile and install ondev2 for postmarketOS
# https://postmarketos.org/pmb-ci

if [ "$(id -u)" = 0 ]; then
	set -x
	apk -q add \
		abuild \
		build-base \
		cryptsetup-dev \
		eudev-dev \
		gettext-tiny \
		git \
		inih-dev \
		libdrm-dev \
		libinput-dev \
		libxkbcommon-dev \
		linux-headers \
		meson \
		musl-libintl \
		pkgconf \
		util-linux-dev
	exec su "${TESTUSER:-build}" -c "sh -e $0"
fi

set -x

# Use -Dunl0kr:werror=false, otherwise it builds lv_drivers with
# -Werror=pedantic and fails as of writing. Setting warning_level for
# subprojects didn't work for me.
abuild-meson . build -Dwerror=true -Dunl0kr:werror=false
meson compile -C build

mkdir -p ~/_install
DESTDIR=~/_install meson install --no-rebuild -C build
