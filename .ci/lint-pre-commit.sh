#!/bin/sh -e
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith
# Run checkpatch against uncommitted changes. This is faster than running it
# against the whole tree, useful as git pre-commit hook.
SCRIPT_DIR="$(dirname "$(realpath "$0")")"

cd "$SCRIPT_DIR/.."
if [ ! -e .ci/checkpatch.pl ]; then
	echo "ERROR: .ci/checkpatch.pl is missing."
	echo "Run .ci/lint.sh once to download it."
	exit 1
fi

if [ -n "$(git status --porcelain)" ]; then
	COMMIT="HEAD"
else
	COMMIT="HEAD~1"
fi

echo "Running checkpatch..."
if ! git diff -U0 "$COMMIT" | .ci/checkpatch.pl - \
	--color=always \
	--mailback \
	--show-types \
	--showfile \
	--terse
then
	exit 1
fi
