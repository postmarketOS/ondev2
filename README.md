# ondev2

Experimental new version of the
[on-device installer](https://gitlab.com/postmarketOS/postmarketos-ondev/) with
new features:

* Support for localizations
* All install screens now available in a text-based interface too
  * To be used with screen readers/text to speech for accessibility
  * Useful for CI too
* Using the [unl0kr](https://gitlab.com/cherrypicker/unl0kr) keyboard
  * Solves problem of having up to 3 different keyboards (installer, FDE, UI)
* Simple and advanced installation mode
* Builds much faster, should be easier to develop and maintain going forward
* Minimal dependencies -> smaller installer images
* Adaptive interface

## Screenshots / CLI interface

Running on the PinePhone, changing the language:

![ondev2-pinephone](https://gitlab.com/postmarketOS/ondev2/uploads/12f865a513efedc332c6958b9cdcb89d/ondev2-pinephone.jpg)

Running on a laptop:

![ondev2-laptop](https://gitlab.com/postmarketOS/ondev2/uploads/a3bea6489cdf6e1ffc3ff92bb6c37dc5/ondev2-laptop.jpg)

While the above screen is displayed, on CLI:

```
:: Advanced Options
Do you want to configure advanced options  (hostname, filesystem, ...)?

0: Go Back
1: Simple Installation
2: Advanced Installation

What now?>
```

## Status

Currently this needs some WIP branches to work with postmarketOS:

* pmbootstrap: ondev2 branch
* pmaports: ondev2 branch
* boot-deploy: MR 20

See the
[FOSDEM '23](https://fosdem.org/2023/schedule/event/ondev2_installer/)
talk for an introduction to the project.
