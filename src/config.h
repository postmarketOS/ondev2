/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once
#include <stdint.h>

#define ONDEV2_VERSION "2.0.0 WIP"

/* All config options are described in defaults.cfg. */
struct od2_config {
	char *device_arch;
	char *device_name;

	char *device_storage_emmc;
	char *device_storage_emmc_repartition;
	char *device_storage_nvme;
	char *device_storage_nvme_repartition;
	char *device_storage_sd;

	char *device_partition_type;
	char *device_boot_part_start;
	char *device_boot_part_size;
	char *device_boot_filesystem;

	char *device_cgpt_kpart;
	char *device_cgpt_kpart_start;
	char *device_cgpt_kpart_size;

	char *os_name;
	char *os_version;

	char *user_interface;

	char *update_fstab_crypttab;

	char *cmd_luksformat;
	char *cmd_mkfs_root_ext4;
	char *cmd_mount_installdata;

	char *cmd_post_install;
	char *cmd_reboot;
	char *cmd_poweroff;

	char *mountpoint;
};

int od2_config_load(const char *path);

/* Convenience macros for accessing the config options. Note that we don't need
 * to check for NULL as all config keys are defined in defaults.cfg, and we
 * load the value from there first (and overwrite it from the user's config if
 * it is set there). */
#define od2_config_is_empty(key) \
	(strcmp(g_config.key, "") == 0)
#define od2_config_is_true(key) \
	(strcmp(g_config.key, "true") == 0)

extern struct od2_config g_config;
