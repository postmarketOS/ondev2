// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith
#include "config.h"
#include "storages.h"
#include <assert.h>
#include <blkid/blkid.h>
#include <ctype.h>
#include <errno.h>
#include <mntent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_DEVICE_PATH_LEN 50
#define MAX_PARTITION_PATH_LEN 52

struct od2_storage g_storages[_OD2_STORAGE_MAX] = {
	{
		.id = _OD2_STORAGE_UNKNOWN,
		.name = "ERROR: unknown storage, add it to the config",
	},
	{
		.id = OD2_STORAGE_EMMC,
		.name = "eMMC",
	},
	{
		.id = OD2_STORAGE_NVME,
		.name = "NVMe",
	},
	{
		.id = OD2_STORAGE_SD,
		.name = "SD",
	},
};

enum od2_storage_id g_boot_dev_id;

const char *od2_storage_get_path(enum od2_storage_id id)
{
	switch (id) {
	case OD2_STORAGE_EMMC:
		return g_config.device_storage_emmc;
	case OD2_STORAGE_NVME:
		return g_config.device_storage_nvme;
	case OD2_STORAGE_SD:
		return g_config.device_storage_sd;
	default:
		return "/error-invalid-storage-passed-to-od2_storage_get_path";
	}
}

static enum od2_storage_id get_id(const char *path)
{
	enum od2_storage_id id;
	const char *path_id;

	for (id = _OD2_STORAGE_UNKNOWN + 1; id < _OD2_STORAGE_MAX; id++) {
		path_id = od2_storage_get_path(id);
		if (strcmp(path_id, path) == 0)
			return id;
	}

	return _OD2_STORAGE_UNKNOWN;
}

static const char *get_boot_dev(void)
{
	static char buf[MAX_DEVICE_PATH_LEN] = {0};
	size_t buf_len;
	const char *filename = getenv("OD2_PROC_SELF_MOUNTS");
	struct mntent *mntent;
	FILE *f;

	if (!filename)
		filename = "/proc/self/mounts";

	/* Copy partition path at mountpoint to buf (e.g. /dev/mmcblk0p2) */
	f = setmntent(filename, "r");
	if (!f) {
		perror("ERROR: get_boot_dev: setmntent failed");
		return NULL;
	}
	while ((mntent = getmntent(f))) {
		if (strcmp(g_config.mountpoint, mntent->mnt_dir) != 0)
			continue;

		assert(strlen(mntent->mnt_fsname) < sizeof(buf) - 1);
		strncpy(buf, mntent->mnt_fsname, sizeof(buf) - 1);
		break;
	}

	endmntent(f);

	/* Shortest valid partition name has 8 characters: /dev/sda1 */
	buf_len = strlen(buf);
	if (buf_len < 8) {
		printf("ERROR: get_boot_dev: '%s' is not a valid root partition\n", buf);
		return NULL;
	}

	if (!isdigit(buf[buf_len - 1])) {
		printf("ERROR: get_boot_dev: '%s' doesn't end with partition digit\n", buf);
		return NULL;
	}

	/* Cut off the partition digit and 'p' letter (e.g. /dev/sda,
	 * /dev/mmcblk0). More than 9 partitions seems unlikely. */
	buf[buf_len - 1] = '\0';
	if (buf[buf_len - 2] == 'p')
		buf[buf_len - 2] = '\0';
	else
		buf[buf_len - 1] = '\0';

	return buf;
}

static const char *partsep(const char *dev)
{
	/* Devices ending with a number have 'p' as separator, e.g.
	 * /dev/mmcblk0p1. Otherwise they don't. See add_partition() in
	 * block/partitions/core.c in linux.git. */
	if (isdigit(dev[strlen(dev) - 1]))
		return "p";
	else
		return "";
}

const char *od2_storage_get_boot_partition(enum od2_storage_id target_id)
{
	const char *install_dev = od2_storage_get_path(target_id);
	static char buf[MAX_PARTITION_PATH_LEN] = {0};

	/* For now, always the first partition on the install_dev. */
	snprintf(buf, sizeof(buf), "%s%s1", install_dev, partsep(install_dev));

	return buf;
}

const char *od2_storage_get_root_partition(enum od2_storage_id target_id)
{
	const char *install_dev = od2_storage_get_path(target_id);
	static char buf[MAX_PARTITION_PATH_LEN] = {0};

	/* For now, always the second partition on the install_dev. */
	snprintf(buf, sizeof(buf), "%s%s2", install_dev, partsep(install_dev));

	return buf;
}

const char *od2_storage_get_install_data_partition(void)
{
	const char *boot_dev = get_boot_dev();
	static char buf[MAX_PARTITION_PATH_LEN] = {0};

	/* Install data is always on the third partition of the boot device:
	 * [boot part][free space][install data] */
	snprintf(buf, sizeof(buf), "%s%s3", boot_dev, partsep(boot_dev));

	return buf;
}

static void set_available(enum od2_storage_id id)
{
	if (id == _OD2_STORAGE_UNKNOWN)
		return;

	if (access(od2_storage_get_path(id), F_OK) == 0)
		g_storages[id].available = true;
}

static void print_summary(const char *boot_dev, enum od2_storage_id boot_id)
{
	enum od2_storage_id id;

	printf("Booted from:\n");
	printf("  %s (%s)\n", boot_dev, g_storages[boot_id].name);

	printf("Available installation target(s):\n");
	for (id = _OD2_STORAGE_UNKNOWN + 1; id < _OD2_STORAGE_MAX; id++) {
		if (!g_storages[id].available)
			continue;

		printf("  %s (%s)\n", od2_storage_get_path(id),
		       g_storages[id].name);
	}

	printf("\n");
}

int od2_storage_init(void)
{
	const char *boot_dev = get_boot_dev();

	if (!boot_dev)
		return -EINVAL;

	g_boot_dev_id = get_id(boot_dev);

	/* Boot from any storage: allow install to itself */
	set_available(g_boot_dev_id);

	/* Boot from external storage: allow install to internal storages */
	switch (g_boot_dev_id) {
	case OD2_STORAGE_SD:
		set_available(OD2_STORAGE_EMMC);
		set_available(OD2_STORAGE_NVME);
		break;
	default:
		/* assume booted from internal storage */
		break;
	}

	print_summary(boot_dev, g_boot_dev_id);

	if (g_boot_dev_id == _OD2_STORAGE_UNKNOWN)
		return -EINVAL;

	if (od2_storage_count_available() == 0) {
		printf("ERROR: no install storage found, passed a bogus"
		       " OD2_PROC_SELF_MOUNTS with non-existing devices?\n");
		return -EINVAL;
	}

	return 0;
}

int od2_storage_count_available(void)
{
	int ret = 0;
	enum od2_storage_id id;

	for (id = _OD2_STORAGE_UNKNOWN + 1; id < _OD2_STORAGE_MAX; id++) {
		if (g_storages[id].available)
			ret++;
	}

	return ret;
}

bool od2_storage_is_boot_dev(enum od2_storage_id id)
{
	return g_boot_dev_id == id;
}

enum od2_storage_id od2_storage_get_boot_dev(void)
{
	return g_boot_dev_id;
}

bool od2_storage_repartition(enum od2_storage_id id)
{
	switch (id) {
	case OD2_STORAGE_EMMC:
		return od2_config_is_true(device_storage_emmc_repartition);
	case OD2_STORAGE_NVME:
		return od2_config_is_true(device_storage_nvme_repartition);
	default:
		return false;
	}
}

const char *od2_storage_get_uuid_str(const char *path)
{
	blkid_probe pr;
	static char buf[100] = "UUID=";
	const char *uuid;

	pr = blkid_new_probe_from_filename(path);
	if (!pr) {
		printf("ERROR: failed to get UUID of %s\n", path);
		return NULL;
	}

	if (blkid_do_probe(pr) == -1) {
		printf("ERROR: blkid_do_probe failed on %s\n", path);
		return NULL;
	}

	if (blkid_probe_lookup_value(pr, "UUID", &uuid, NULL) == -1) {
		printf("ERROR: blkid_proble_lookup_value failed on %s\n", path);
		return NULL;
	}

	blkid_free_probe(pr);

	snprintf(buf, sizeof(buf), "UUID=%s", uuid);
	return buf;
}
