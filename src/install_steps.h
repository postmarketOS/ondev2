/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once

int od2_install_step_mount_install_data(void);
int od2_install_step_create_partition_table_cgpt(void);
int od2_install_step_create_partition_table_regular(void);
int od2_install_step_format_boot_partition_fs(void);
int od2_install_step_format_root_partition_luks(void);
int od2_install_step_mount_root_partition_luks(void);
int od2_install_step_format_root_partition_fs(void);
int od2_install_step_mount_root(void);
int od2_install_step_copy_files_root(void);
int od2_install_step_copy_files_root_progress(void);
int od2_install_step_mount_boot(void);
int od2_install_step_copy_files_boot(void);
int od2_install_step_update_fstab(void);
int od2_install_step_update_crypttab(void);
int od2_install_step_prepare_chroot(void);
int od2_install_step_run_post_install_script(void);
int od2_install_step_clean_chroot(void);
int od2_install_step_umount_boot_root_install_data(void);
int od2_install_step_umount_root_partition_luks(void);
int od2_install_step_extend_root_over_install_data(void);
