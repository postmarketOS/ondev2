// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith
#include "config.h"
#include "pages.h"
#include "ui.h"
#include <libintl.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void show_usage(void)
{
	printf("usage: ondev2 [-h|CONFIG]\n");
	printf("\n");
	printf("arguments:\n");
	printf("  -h     show this help message and exit\n");
	printf("  CONFIG path to config (default: /etc/ondev2.cfg)\n");
}

static int parse_args(const char **path, int argc, const char **argv)
{
	*path = "/etc/ondev2.cfg";

	if (argc == 2) {
		if (strcmp(argv[1], "-h") == 0) {
			show_usage();
			return -1;
		}
		*path = argv[1];
	}
	return 0;
}

int main(int argc, const char **argv)
{
	const char *path;

	/* Set OD2=1, so shell scripts know they were called from ondev2 */
	setenv("OD2", "1", 1);

	if (parse_args(&path, argc, argv) < 0)
		return 1;
	if (od2_config_load(path) < 0)
		return 1;

	setlocale(LC_ALL, "");
	bindtextdomain("ondev2", "/usr/share/locale");
	textdomain("ondev2");

	od2_ui_init(OD2_PAGE_LANGUAGE);
	od2_ui_interact();
}
