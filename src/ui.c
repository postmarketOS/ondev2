// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith
#include "cmd.h"
#include "config.h"
#include "install.h"
#include "pages.h"
#include "storages.h"
#include "ui.h"
#include "ui_cli.h"
#include "ui_gui.h"

#include "unl0kr/terminal.h"

#include <assert.h>
#include <locale.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Array of pages previously shown to the user */
enum od2_page_id g_history[_OD2_PAGE_MAX];
uint8_t g_history_num;

#define page_id_current() g_history[g_history_num - 1]
#define page_id_prev() g_history[g_history_num - 2]
#define page_id_prev_prev() g_history[g_history_num - 3]

struct od2_page *od2_ui_page_current(void)
{
	return &g_pages[page_id_current()];
}

struct od2_page *od2_ui_page_prev(void)
{
	return &g_pages[page_id_prev()];
}

struct od2_page *od2_ui_page_prev_prev(void)
{
	return &g_pages[page_id_prev_prev()];
}

void od2_ui_show_page(void)
{
	struct od2_page *page = od2_ui_page_current();

	od2_ui_cli_show_page(page);
	od2_ui_gui_show_page(page);
}

void od2_ui_interact_once(void)
{
	struct od2_page *page = od2_ui_page_current();

	od2_ui_cli_interact(page);
	od2_ui_gui_interact();
}

void od2_ui_interact(void)
{
	while (true) {
		od2_ui_interact_once();
		usleep(INTERACT_LOOP_SLEEP_MS);
	}
}

static void navigate_to(enum od2_page_id id)
{
	g_history[g_history_num] = id;
	g_history_num++;

	od2_ui_show_page();
}

static void navigate_back(void)
{
	g_history_num--;

	od2_ui_show_page();
}

static void navigate_to_if_pass_matches(enum od2_page_id id)
{
	if (strcmp(od2_ui_page_current()->input_text,
		   od2_ui_page_prev()->input_text) == 0)
		return navigate_to(id);

	navigate_to(OD2_PAGE_ERROR_PASS_DOESNT_MATCH);
}

static void navigate_to_storage(void)
{
	od2_page_storage_update_buttons();
	navigate_to(OD2_PAGE_STORAGE);
}

static void sigaction_handler(int signum)
{
	LV_UNUSED(signum);
	ul_terminal_reset_current_terminal();
	exit(0);
}

void od2_ui_init(enum od2_page_id initial)
{
	struct sigaction action = {.sa_handler = sigaction_handler};
	enum od2_page_id id;
	struct od2_page *page;

	printf("\n");
	printf("ondev2 (%s)\n", ONDEV2_VERSION);
	printf("\n");

	if (od2_storage_init() < 0)
		initial = OD2_PAGE_ERROR_INSTALL_FAILED;

	for (id = 0; id < _OD2_PAGE_MAX; id++) {
		page = &g_pages[id];
		assert(page->id == id);

		if (page->dropdown)
			page->dropdown_selected = &page->dropdown[0];

		if (page->input == OD2_INPUT_TEXT)
			page->input_text = strdup("");
	}

	/* Initialize terminal + sigactions here, affects ui_cli and ui_gui */
	ul_terminal_prepare_current_terminal(true, false);
	sigaction(SIGINT, &action, NULL);
	sigaction(SIGTERM, &action, NULL);

	od2_ui_gui_init();

	navigate_to(initial);
}

static bool is_simple_selected(void)
{
	return g_pages[OD2_PAGE_SIMPLE_OR_ADVANCED].button_selected
		== OD2_BUTTON_SIMPLE;
}

static bool is_translation_selected(void)
{
	const char *id = g_pages[OD2_PAGE_LANGUAGE].dropdown_selected->id;

	return strcmp(id, "en") != 0;
}

bool od2_ui_is_fde_selected(void)
{
	if (is_simple_selected())
		return true;
	return g_pages[OD2_PAGE_ADVANCED_FDE].button_selected == OD2_BUTTON_ENABLE;
}

static void navigate_to_simple_or_advanced(void)
{
	if (is_simple_selected())
		return navigate_to(OD2_PAGE_SIMPLE_USER_FDE_PASS_COMBINED);
	return navigate_to(OD2_PAGE_ADVANCED_FILESYSTEM);
}

static void navigate_to_overwrite_warning_or_skip(void)
{
	enum od2_storage_id target_id = od2_ui_get_target_storage();

	/* Skip overwrite warning if ondev2 was flashed to the same device that
	 * we are installing to. We will just install into the empty space
	 * before the install data. */
	if (od2_storage_is_boot_dev(target_id))
		return navigate_to_simple_or_advanced();

	return navigate_to(OD2_PAGE_OVERWRITE_WARNING);
}

static void navigate_to_password_try_again(void)
{
	struct od2_page *prev = od2_ui_page_prev();
	struct od2_page *prev_prev = od2_ui_page_prev_prev();

	/* Clear previous password attempts */
	free(prev->input_text);
	prev->input_text = strdup("");

	free(prev_prev->input_text);
	prev_prev->input_text = strdup("");

	/* Go two pages back, so the user can attempt to type in the password
	 * right two times in a row again. */
	g_history_num -= 2;
	od2_ui_show_page();
}

void od2_ui_navigate_to_finished(void)
{
	enum od2_storage_id boot_dev = od2_storage_get_boot_dev();
	enum od2_storage_id target_dev = od2_ui_get_target_storage();

	if (boot_dev == OD2_STORAGE_SD && target_dev != boot_dev)
		return navigate_to(OD2_PAGE_FINISHED_REMOVE_SD);

	navigate_to(OD2_PAGE_FINISHED);
}

static void confirm_finished_page(bool reboot)
{
	const char *cmd = reboot ? g_config.cmd_reboot : g_config.cmd_poweroff;

	if (run_cmd(cmd) != 0)
		od2_ui_navigate_to_failed();
}

void od2_ui_navigate_to_failed(void)
{
	navigate_to(OD2_PAGE_ERROR_INSTALL_FAILED);
}

void od2_ui_on_button(enum od2_button id)
{
	struct od2_page *page = od2_ui_page_current();

	if (id == OD2_BUTTON_BACK)
		return navigate_back();

	/* Don't allow to continue on input pages if the input is empty. */
	if (page->input == OD2_INPUT_TEXT && !strcmp(page->input_text, ""))
		return;

	page->button_selected = id;

	switch (page_id_current()) {
	case OD2_PAGE_LANGUAGE:
		if (is_translation_selected())
			return navigate_to(OD2_PAGE_TRANSLATION_INCOMPLETE);
		return navigate_to(OD2_PAGE_SIMPLE_OR_ADVANCED);
	case OD2_PAGE_TRANSLATION_INCOMPLETE:
		return navigate_to(OD2_PAGE_SIMPLE_OR_ADVANCED);
	case OD2_PAGE_SIMPLE_OR_ADVANCED:
		if (od2_storage_count_available() > 1)
			return navigate_to_storage();
		return navigate_to_simple_or_advanced();
	case OD2_PAGE_STORAGE:
		return navigate_to_overwrite_warning_or_skip();
	case OD2_PAGE_OVERWRITE_WARNING:
		return navigate_to_simple_or_advanced();
	case OD2_PAGE_SIMPLE_USER_FDE_PASS_COMBINED:
		return navigate_to(OD2_PAGE_SIMPLE_USER_FDE_PASS_COMBINED_CONFIRM);
	case OD2_PAGE_SIMPLE_USER_FDE_PASS_COMBINED_CONFIRM:
		return navigate_to_if_pass_matches(OD2_PAGE_FINAL_CONFIRM);
	case OD2_PAGE_ADVANCED_FILESYSTEM:
		return navigate_to(OD2_PAGE_ADVANCED_USER_PASS);
	case OD2_PAGE_ADVANCED_USER_PASS:
		return navigate_to(OD2_PAGE_ADVANCED_USER_PASS_CONFIRM);
	case OD2_PAGE_ADVANCED_USER_PASS_CONFIRM:
		return navigate_to_if_pass_matches(OD2_PAGE_ADVANCED_FDE);
	case OD2_PAGE_ADVANCED_FDE:
		if (od2_ui_is_fde_selected())
			return navigate_to(OD2_PAGE_ADVANCED_FDE_USER_PASS_OR_NOT);
		return navigate_to(OD2_PAGE_FINAL_CONFIRM);
	case OD2_PAGE_ADVANCED_FDE_USER_PASS_OR_NOT:
		if (id == OD2_BUTTON_FDE_USER_PASS)
			return navigate_to(OD2_PAGE_FINAL_CONFIRM);
		return navigate_to(OD2_PAGE_ADVANCED_FDE_PASS);
	case OD2_PAGE_ADVANCED_FDE_PASS:
		return navigate_to(OD2_PAGE_ADVANCED_FDE_PASS_CONFIRM);
	case OD2_PAGE_ADVANCED_FDE_PASS_CONFIRM:
		return navigate_to_if_pass_matches(OD2_PAGE_FINAL_CONFIRM);
	case OD2_PAGE_FINAL_CONFIRM:
		navigate_to(OD2_PAGE_WAIT);
		return od2_install_start();
	case OD2_PAGE_FINISHED:
		return confirm_finished_page(true);
	case OD2_PAGE_FINISHED_REMOVE_SD:
		return confirm_finished_page(false);
	case OD2_PAGE_ERROR_PASS_DOESNT_MATCH:
		return navigate_to_password_try_again();
	default:
		printf("STUB: handling buttons on this page is not implemented.\n");
		return;
	}
}

void od2_ui_on_button_default(void)
{
	struct od2_page *page = od2_ui_page_current();
	enum od2_button btn = OD2_BUTTON_NONE;
	uint8_t i;

	for (i = 0; page->buttons[i] != OD2_BUTTON_NONE; i++) {
		/* back button is never the default button */
		if (page->buttons[i] == OD2_BUTTON_BACK)
			continue;

		/* found more than one button -> do nothing */
		if (btn != OD2_BUTTON_NONE)
			return;

		btn = page->buttons[i];
	}

	if (btn != OD2_BUTTON_NONE)
		od2_ui_on_button(btn);
}

void od2_ui_on_dropdown(struct od2_dropdown *dropdown)
{
	struct od2_page *page = od2_ui_page_current();

	switch (page->id) {
	case OD2_PAGE_LANGUAGE:
		setlocale(LC_ALL, dropdown->value);
		break;
	default:
		break;
	}

	page->dropdown_selected = dropdown;
	od2_ui_show_page();
}

enum od2_storage_id od2_ui_get_target_storage(void)
{
	enum od2_storage_id ret;

	if (od2_storage_count_available() > 1) {
		ret = (enum od2_storage_id)g_pages[OD2_PAGE_STORAGE].button_selected;

		if (ret <= _OD2_STORAGE_UNKNOWN || ret >= _OD2_STORAGE_MAX) {
			printf("ERROR: od2_ui_get_target_storage: invalid id: %i\n",
			       ret);
			return _OD2_STORAGE_UNKNOWN;
		}

		return ret;
	}

	for (ret = _OD2_STORAGE_UNKNOWN + 1; ret < _OD2_STORAGE_MAX; ret++) {
		if (g_storages[ret].available)
			return ret;
	}

	printf("ERROR: od2_ui_get_target_storage: no result\n");
	return _OD2_STORAGE_UNKNOWN;
}

const char *od2_ui_get_fde_pass(void)
{
	/* Simple mode: there is just one password */
	if (is_simple_selected())
		return g_pages[OD2_PAGE_SIMPLE_USER_FDE_PASS_COMBINED].input_text;

	/* Advanced mode: using user's password for FDE too */
	if (g_pages[OD2_PAGE_ADVANCED_FDE_USER_PASS_OR_NOT].button_selected
			== OD2_BUTTON_FDE_USER_PASS)
		return g_pages[OD2_PAGE_ADVANCED_USER_PASS].input_text;

	/* Advanced mode: separate FDE password */
	return g_pages[OD2_PAGE_ADVANCED_FDE_PASS].input_text;
}
