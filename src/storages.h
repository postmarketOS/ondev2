/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once
#include <stdbool.h>

/* Keep in sync with enum od2_button */
enum od2_storage_id {
	_OD2_STORAGE_UNKNOWN,
	OD2_STORAGE_EMMC,
	OD2_STORAGE_NVME,
	OD2_STORAGE_SD,
	_OD2_STORAGE_MAX,
};

struct od2_storage {
	enum od2_storage_id id;
	const char *name;
	bool available;
};

extern struct od2_storage g_storages[_OD2_STORAGE_MAX];

const char *od2_storage_get_path(enum od2_storage_id id);
const char *od2_storage_get_boot_partition(enum od2_storage_id target_id);
const char *od2_storage_get_root_partition(enum od2_storage_id target_id);
const char *od2_storage_get_install_data_partition(void);

int od2_storage_init(void);
int od2_storage_count_available(void);
bool od2_storage_is_boot_dev(enum od2_storage_id id);
enum od2_storage_id od2_storage_get_boot_dev(void);
bool od2_storage_repartition(enum od2_storage_id id);

const char *od2_storage_get_uuid_str(const char *path);
