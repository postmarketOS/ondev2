// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith
#include "config.h"
#include "install.h"
#include "install_steps.h"
#include "misc.h"
#include "pages.h"
#include "ui.h"
#include "ui_gui.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pthread.h>

struct {
	pthread_barrier_t new_step;
	struct od2_install_step *step;
	pthread_t thread;
	int ret;
	volatile bool step_done;
} g_worker_thread = {
	.ret = 0,
	.step_done = false,
};

struct od2_install_step g_install_steps[_OD2_INSTALL_STEP_MAX] = {
	{
		.id = OD2_INSTALL_STEP_MOUNT_INSTALL_DATA,
		.func = od2_install_step_mount_install_data,
		.name = "mount-install-data",
		.enabled = true,
	},
	{
		.id = OD2_INSTALL_STEP_CREATE_PARTITION_TABLE_CGPT,
		.func = od2_install_step_create_partition_table_cgpt,
		.name = "create-partition-table-cgpt",
	},
	{
		.id = OD2_INSTALL_STEP_CREATE_PARTITION_TABLE_REGULAR,
		.func = od2_install_step_create_partition_table_regular,
		.name = "create-partition-table-regular",
	},
	{
		.id = OD2_INSTALL_STEP_FORMAT_BOOT_PARTITION_FS,
		.func = od2_install_step_format_boot_partition_fs,
		.name = "format-boot-partition-fs",
	},
	{
		.id = OD2_INSTALL_STEP_PREPARE_LVM,
		.name = "prepare-lvm",
	},
	{
		.id = OD2_INSTALL_STEP_FORMAT_ROOT_PARTITION_LUKS,
		.func = od2_install_step_format_root_partition_luks,
		.name = "format-root-partition-luks",
		.weight = 13,
	},
	{
		.id = OD2_INSTALL_STEP_MOUNT_ROOT_PARTITION_LUKS,
		.func = od2_install_step_mount_root_partition_luks,
		.name = "mount-root-partition-luks",
		.weight = 5,
	},
	{
		.id = OD2_INSTALL_STEP_FORMAT_ROOT_PARTITION_FS,
		.func = od2_install_step_format_root_partition_fs,
		.name = "format-root-partition-fs",
		.enabled = true,
		.weight = 10,
	},
	{
		.id = OD2_INSTALL_STEP_MOUNT_ROOT,
		.func = od2_install_step_mount_root,
		.name = "mount-root",
		.enabled = true,
	},
	{
		.id = OD2_INSTALL_STEP_COPY_FILES_ROOT,
		.func = od2_install_step_copy_files_root,
		.func_progress = od2_install_step_copy_files_root_progress,
		.name = "copy-files-root",
		.enabled = true,
		.weight = 50,
	},
	{
		.id = OD2_INSTALL_STEP_MOUNT_BOOT,
		.func = od2_install_step_mount_boot,
		.name = "mount-boot",
		.enabled = true,
	},
	{
		.id = OD2_INSTALL_STEP_COPY_FILES_BOOT,
		.func = od2_install_step_copy_files_boot,
		.name = "copy-files-boot",
		.enabled = true,
	},
	{
		.id = OD2_INSTALL_STEP_UPDATE_FSTAB,
		.func = od2_install_step_update_fstab,
		.name = "update-fstab",
	},
	{
		.id = OD2_INSTALL_STEP_UPDATE_CRYPTTAB,
		.func = od2_install_step_update_crypttab,
		.name = "update-crypttab",
	},
	{
		.id = OD2_INSTALL_STEP_PREPARE_CHROOT,
		.func = od2_install_step_prepare_chroot,
		.name = "prepare-chroot",
	},
	{
		.id = OD2_INSTALL_STEP_RUN_POST_INSTALL_SCRIPT,
		.func = od2_install_step_run_post_install_script,
		.name = "run-post-install-script",
	},
	{
		.id = OD2_INSTALL_STEP_CLEAN_CHROOT,
		.func = od2_install_step_clean_chroot,
		.name = "clean-chroot",
	},
	{
		.id = OD2_INSTALL_STEP_UMOUNT_BOOT_ROOT_INSTALL_DATA,
		.func = od2_install_step_umount_boot_root_install_data,
		.name = "umount-boot-root-install-data",
		.enabled = true,
	},
	{
		.id = OD2_INSTALL_STEP_UMOUNT_ROOT_PARTITION_LUKS,
		.func = od2_install_step_umount_root_partition_luks,
		.name = "umount-root-partition-luks",
	},
	{
		.id = OD2_INSTALL_STEP_EXTEND_ROOT_OVER_INSTALL_DATA,
		.func = od2_install_step_extend_root_over_install_data,
		.name = "extend-root-over-install-data",
		.weight = 10,
	},
};

static void step_enable(enum od2_install_step_id id)
{
	g_install_steps[id].enabled = true;
}

static int steps_init(void)
{
	enum od2_storage_id target_id = od2_ui_get_target_storage();
	bool use_chroot = false;

	if (target_id == _OD2_STORAGE_UNKNOWN)
		return -EINVAL;

	if (od2_storage_is_boot_dev(target_id)) {
		/* Same storage install, e.g. SD -> SD */
		step_enable(OD2_INSTALL_STEP_EXTEND_ROOT_OVER_INSTALL_DATA);
	} else {
		/* Different storage install, e.g. SD -> eMMC */
		if (od2_storage_repartition(target_id)) {
			if (od2_config_is_empty(device_cgpt_kpart))
				step_enable(OD2_INSTALL_STEP_CREATE_PARTITION_TABLE_REGULAR);
			else
				step_enable(OD2_INSTALL_STEP_CREATE_PARTITION_TABLE_CGPT);

			step_enable(OD2_INSTALL_STEP_FORMAT_BOOT_PARTITION_FS);
		} else {
			step_enable(OD2_INSTALL_STEP_PREPARE_LVM);
			printf("ERROR: installation to same storage without"
			       " repartition is not implemented yet.\n");
			return -EINVAL;
		}
	}

	if (od2_ui_is_fde_selected()) {
		step_enable(OD2_INSTALL_STEP_FORMAT_ROOT_PARTITION_LUKS);
		step_enable(OD2_INSTALL_STEP_MOUNT_ROOT_PARTITION_LUKS);
		step_enable(OD2_INSTALL_STEP_UMOUNT_ROOT_PARTITION_LUKS);
	}

	if (!od2_config_is_empty(update_fstab_crypttab)) {
		step_enable(OD2_INSTALL_STEP_UPDATE_FSTAB);
		if (od2_ui_is_fde_selected())
			step_enable(OD2_INSTALL_STEP_UPDATE_CRYPTTAB);
	}

	if (!od2_config_is_empty(cmd_post_install)) {
		step_enable(OD2_INSTALL_STEP_RUN_POST_INSTALL_SCRIPT);
		use_chroot = true;
	}

	if (use_chroot) {
		step_enable(OD2_INSTALL_STEP_PREPARE_CHROOT);
		step_enable(OD2_INSTALL_STEP_CLEAN_CHROOT);
	}

	return 0;
}

static void steps_print_summary(void)
{
	enum od2_install_step_id id;

	printf("Enabled install steps:\n");
	for (id = 0; id < _OD2_INSTALL_STEP_MAX; id++) {
		if (g_install_steps[id].enabled)
			printf("  %s\n", g_install_steps[id].name);
	}

	printf("Disabled install steps:\n");
	for (id = 0; id < _OD2_INSTALL_STEP_MAX; id++) {
		if (!g_install_steps[id].enabled)
			printf("  %s\n", g_install_steps[id].name);
	}

	printf("\n");
}

/* Calculate fake progress based on time passed. */
static uint8_t progress_time(struct od2_install_step *step,
			     struct timeval *start)
{
	struct timeval now;
	double passed_s, finished_s;

	gettimeofday(&now, NULL);

	passed_s = (now.tv_sec - start->tv_sec) +
		   ((now.tv_usec - start->tv_usec) / 1e6);

	finished_s = step->weight / 2;

	if (passed_s < finished_s)
		return 100 * passed_s / finished_s;

	return 100;
}

static void steps_update_progress(enum od2_install_step_id current,
				  uint8_t step_percent)
{
	uint16_t weight_total = 0;
	uint16_t weight_passed = 0;
	uint16_t weight_step;

	enum od2_install_step_id id;
	for (id = 0; id < _OD2_INSTALL_STEP_MAX; id++) {
		struct od2_install_step *step = &g_install_steps[id];

		if (!step->enabled)
			continue;

		weight_step = step->weight ? step->weight : 1;

		weight_total += weight_step;
		if (id < current)
			weight_passed += weight_step;
		else if (id == current)
			weight_passed += weight_step * step_percent / 100;
	}

	od2_ui_page_current()->progress = 100 * weight_passed / weight_total;
	od2_ui_show_page();
	od2_ui_interact_once();
}

static void *step_worker(void *arg)
{
	UNUSED(arg);

	while (true) {
		pthread_barrier_wait(&g_worker_thread.new_step);

		struct od2_install_step *step = g_worker_thread.step;
		if (!step)
			break;

		g_worker_thread.ret = step->func();
		g_worker_thread.step_done = true;
	}

	return NULL;
}

static int steps_run_step(struct od2_install_step *step)
{
	struct timeval start;

	printf("Running step: %s\n", step->name);
	steps_update_progress(step->id, 0);

	if (!step->func) {
		printf("ERROR: step %s doesn't have an implementation function!\n",
		       step->name);
		return -1;
	}

	if (!step->func_progress)
		gettimeofday(&start, NULL);

	g_worker_thread.step = step;
	g_worker_thread.step_done = false;
	g_worker_thread.ret = 0;
	pthread_barrier_wait(&g_worker_thread.new_step);

	/* Main process: update UI while waiting for completion */
	while (true) {
		od2_ui_interact_once();
		if (g_worker_thread.step_done)
			break;
		usleep(INTERACT_LOOP_SLEEP_MS);

		if (step->func_progress)
			steps_update_progress(step->id, step->func_progress());
		else
			steps_update_progress(step->id, progress_time(step, &start));
	}

	if (g_worker_thread.ret != 0) {
		printf("ERROR: step %s failed (%i)\n", step->name, g_worker_thread.ret);
		return -1;
	}

	steps_update_progress(step->id, 100);
	return 0;
}

static void steps_run(void)
{
	enum od2_install_step_id id;
	pthread_barrier_init(&g_worker_thread.new_step, NULL, 2);
	pthread_create(&g_worker_thread.thread, NULL, step_worker, NULL);

	for (id = 0; id < _OD2_INSTALL_STEP_MAX; id++) {
		struct od2_install_step *step = &g_install_steps[id];

		if (!step->enabled)
			continue;
		if (steps_run_step(step) < 0)
			goto error;
	}

	od2_ui_navigate_to_finished();
	return;

error:
	od2_ui_navigate_to_failed();
}

void od2_install_start(void)
{
	steps_init();
	steps_print_summary();
	steps_run();
}
